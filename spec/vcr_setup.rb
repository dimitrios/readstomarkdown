require 'vcr'

VCR.configure do |c|
  c.cassette_library_dir = 'spec/vcr_cassettes'
  c.hook_into :webmock

  #
  # https://stackoverflow.com/questions/9816152
  #
  c.filter_sensitive_data('GOODREADS_API_KEY') do
    ENV['GOODREADS_API_KEY']
  end
  c.filter_sensitive_data('GOODREADS_API_SECRET') do
    ENV['GOODREADS_API_SECRET']
  end
end
