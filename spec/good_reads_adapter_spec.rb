require 'vcr_setup'
require 'good_reads_adapter'

# rubocop:disable BlockLength
RSpec.describe GoodReadsAdapter do
  let(:adapter) do
    described_class.new(ENV['GOODREADS_API_KEY'], ENV['GOODREADS_API_SECRET'])
  end

  let(:user_id) { '46258483' }

  let(:shelf_name) { 'read' }

  it 'has retrieve method' do
    expect(adapter).to respond_to(:retrieve)
  end

  context 'Retrieval' do
    it 'retireves books' do
      VCR.use_cassette('dimitrys_books') do
        expect(adapter.retrieve(user_id, shelf_name).size).to be >= 10
      end
    end

    it 'retireves books for specific year' do
      VCR.use_cassette('dimitrys_books') do
        #
        # 2017 is a special year since I (user_id) opened the account
        #
        expect(adapter.retrieve(user_id, shelf_name, 2017).size).to eq 20
      end
    end
  end

  context 'Erroneous Retrieval' do
    let(:adapter) { described_class.new('WRONG', 'WRONG') }

    it 'raises an error with invalid keys' do
      VCR.use_cassette('invalid_api_credentials') do
        expect { adapter.retrieve(user_id, shelf_name) }
          .to raise_error(RuntimeError)
      end
    end
  end
end
# rubocop:enable BlockLength
