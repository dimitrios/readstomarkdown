require 'goodreads'
require 'date'

##
# Adapter class to GoodRead's API
class GoodReadsAdapter
  def initialize(key, secret)
    @key = key
    @secret = secret
    @client = Goodreads::Client.new(api_key: @key, api_secret: @secret)
  end

  def retrieve(user_id, shelf_name, year = nil)
    shelf = client_shelf(user_id, shelf_name)
    all_results = shelf.books.map { |book| filter_book_data(book) }

    if year.blank?
      all_results
    else
      all_results.select { |book| book[:date_added].year == year }
    end
  end

  private

  def client_shelf(user_id, shelf_name)
    @client.shelf(user_id, shelf_name)
  rescue Goodreads::Unauthorized
    raise 'Unauthorized Error, please check API keys'
  end

  def filter_book_data(book)
    #
    # Possible source of failure when authors are more than one:
    #
    author = book[:book][:authors][:author][:name]

    {
      book_title: book[:book][:title],
      image_url: book[:book][:image_url],
      author: author,
      goodreads_link: book[:book][:link],
      date_added:  Date.parse(book[:date_added])
    }
  end
end
