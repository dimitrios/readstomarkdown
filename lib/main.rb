require 'good_reads_adapter'
require 'erb'
require 'date'
require 'optparse'

##
# Orchestrated the application.
class Main
  def initialize(output)
    @output = output
  end

  def run
    # Debug:
    # output.puts("Will work with #{ENV['GOODREADS_API_KEY']} \
    #   and secret: #{ENV['GOODREADS_API_SECRET']}")

    options = options_from_command_line
    # output.puts(options) # Debug

    retriever = GoodReadsAdapter.new(options[:goodreads_api_key],
                                     options[:goodreads_api_secret])
    books = retriever.retrieve(options[:user_id],
                               options[:shelf], options[:year])
    # output.puts(books)

    output.puts(markdown_output(books, options[:year]))
  end

  private

  ##
  # Disabled length check as option parsing is verbose.
  #
  # rubocop:disable MethodLength
  def options_from_command_line
    options = default_options

    OptionParser.new do |opts|
      opts.banner = 'Usage: readstomarkdown [options]'

      opts.on('-year', '--year', OptionParser::DecimalInteger,
              'Year for which to run retrieval') { |y| options[:year] = y.to_i }
      opts.on('-user_id', '--user_id',
              'User ID for retrieval') { |user_id| options[:user_id] = user_id }
      opts.on('-shelf', '--shelf',
              'Shelf to retrieve from') { |shelf| options[:shelf] = shelf }
      opts.on('-goodreads_api_key', '--goodreads_api_key',
              'Goodreads API key') { |key| options[:goodreads_api_key] = key }
      opts.on('-goodreads_api_secret', '--goodreads_api_secret',
              'Goodreads API secret') do |secret|
        options[:goodreads_api_secret] = secret
      end
    end.parse!

    options
  end
  # rubocop:enable MethodLength

  def default_options
    {
      year: Date.today.year,
      user_id: '46258483',
      shelf: 'read',
      goodreads_api_key: ENV['GOODREADS_API_KEY'],
      goodreads_api_secret: ENV['GOODREADS_API_SECRET']
    }
  end

  def markdown_output(books, year)
    b = binding
    b.local_variable_set(:year, year)
    b.local_variable_set(:books, books)

    template.result(b)
  end

  def template
    template_location = File.join('..',
                                  'templates', 'book_list_markdown.md.erb')

    ERB.new(File.read(File.expand_path(template_location, __dir__)))
  end

  attr_reader :output
end
