# Reads to Markdown

Retrieves books read from a specific user on a specified year and outputs them
to markdown format.

## Getting Started

Ruby and Bundler is expected to be installed on our system.


### Installing

After cloning this repository, change into the newly created directory and run

```
bundle install
```

This will install all dependencies needed for the project.


## Running the Tests

All tests can be run by executing

```
bundle exec rspec
```

`rspec` will automatically find all tests inside the `spec` directory and run them.


## Running the Application

To run the application execute ```bin/readstomarkdown``` supplying the required
parameters. Run ```bin/readstomarkdown --help``` for details

A good approach is to have Goodread's API acces keys to environment. To do that
set ```GOODREADS_API_KEY``` and ```GOODREADS_API_SECRET```. A wrapper script
sample, "wrapper.sample" is provided to be used as reference.

## Built With

- [Ruby](https://www.ruby-lang.org/en)
- [Bundler](http://bundler.io)
- [RSpec](http://rspec.info)
- [VCR](https://relishapp.com/vcr/vcr/docs)

## License

This project is licensed under the MIT License - see the [license.md](license.md) file for details.
